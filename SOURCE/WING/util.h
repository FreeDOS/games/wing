/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Util.h																					     *
 *                                                                           *
 * These functions are basic procedures whose purpose is separate from any   *
 * distinct catagorization. 																  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#ifndef UTIL_H
#define UTIL_H

#include <ctype.h>
#include <fstream.h>
#include "additional/apstring.h"
#include "additional/apvector.h"

//==============================================================================
// Prototypes

apstring NumToString ( long int i );
apstring WeaponToString ( WeaponEnum weapon );
WeaponEnum IntToWeapon (int i);
StateEnum IntToState (int i);
EnemyEnum IntToEnemy (int i);

template < class ItemTYPE >
void Swap ( ItemTYPE & left,ItemTYPE & right );
int pow10 ( int num );

apstring GetText ( BITMAP * draw_buffer,char *msgs, char * query, int xcoord, int ycoord,int length );
int GetNum ( BITMAP * draw_buffer,char *msgs, char * query, int xcoord, int ycoord,int length );
bool Confirm 	  ( BITMAP *draw_buffer,char *query,int xcoord,int ycoord);
void LoadStrings (apvector <apstring> & strings, char * file );

#include "util.cpp"      
#endif
