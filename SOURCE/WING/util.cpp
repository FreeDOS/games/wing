//==========================================================================

apstring NumToString ( long int i )
{
	apstring temp_string;
   char temp;
   int temp_num = i;
	while ( temp_num > 0 )
   {
    	temp_string += char (temp_num % 10)+ '0';
      temp_num /= 10;
   }

   for ( int j = 0; j < temp_string.length() / 2; j ++ )
   {
    	temp = temp_string [j];
      temp_string [j] = temp_string [temp_string.length()-j-1];
 		temp_string [temp_string.length()-j-1] = temp;
   }
   if ( i == 0 )
   	temp_string += "0      ";
   return temp_string;
}
//==========================================================================
apstring WeaponToString ( WeaponEnum weapon )
{
	switch ( weapon )
   {
    	case laser   : return "laser";
      case minigun : return "minigun";
      case plasma  : return "plasma";
      case torpedo : return "torpedo";
      default      : return "unknown";
   }
}

//==========================================================================
WeaponEnum IntToWeapon (int i)
{
	switch ( i )
   {
      case 0  : return laser;
      case 1  : return minigun;
      case 2  : return plasma;
      case 3  : return torpedo;
      default : return laser;
   }
}
//==========================================================================
EnemyEnum IntToEnemy (int i)
{
	switch ( i )
   {
      case 0  : return scout;
      case 1  : return fighter;
      case 2  : return bomber;
      case 3  : return destroyer;
      default : return scout;
   }
}
//==========================================================================
StateEnum IntToState (int i)
{
	switch ( i )
   {
      case 0  : return ATTACKING_1;
      case 1  : return ATTACKING_2;
      case 2  : return ATTACKING_3;
      case 3  : return ATTACKING_4;
      case 4  : return ATTACKING_5;
      case 5  : return ATTACKING_6;
   }
}
//==========================================================================
template < class ItemTYPE >
void Swap ( ItemTYPE & left,ItemTYPE & right )
{
	ItemTYPE temp = left;
   left = right;
   right = temp;
}
//==========================================================================
apstring GetText ( BITMAP * draw_buffer, char *msg,char * query, int xcoord, int ycoord,int length )
{
 	char temp;
   int num_letters = 0;
   apstring name_string;
   char temp_string [length];
   clear_keybuf ();

   while ( TRUE )
   {
   	poll_mouse ();
		rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);   // 0 is black
		rect(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 15);		  // 15 is white
	   textprintf_centre(draw_buffer, font, xcoord + 100 , ycoord + 20, 15, msg);
      textprintf(draw_buffer, font, xcoord + 8 , ycoord + 70, 15, query);
      textprintf (draw_buffer,font,xcoord + text_length(font, query)+8, ycoord+70,15, strcpy ( temp_string,name_string.c_str()));
      show_mouse ( draw_buffer );
		blit(draw_buffer, screen, 0, 0, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
      poll_keyboard ();
 	   if ( key [KEY_ENTER] )
      {
        	show_mouse (NULL);
			rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);
         show_mouse (draw_buffer);
     		break;
		}

    	if ( key [KEY_BACKSPACE] && num_letters > 0 )
  	   {
    	   name_string = name_string.substr ( 0, num_letters - 1 );
  			num_letters--;
      }
  		else if ( keypressed ())
 		{
        	temp = (char) readkey () & 0xff;
      	if ( isalnum (temp) && num_letters < length )
         {
  	      	num_letters ++;
				name_string += temp;
			}
		}
      clear_keybuf();
	}
   return name_string.substr (0,length);
}
//==========================================================================
int pow10 ( int num )
{
	int result = 1;	
	for ( int i = 0; i < num; i++ )
		result *= 10;
	return result;		

}
//==========================================================================
int GetNum ( BITMAP * draw_buffer,char *msg, char * query, int xcoord, int ycoord,int length )
{
	int temp_num, count;
   bool valid_num;
   apstring string_num;
	do
   {
     	valid_num = true;
   	string_num = GetText ( draw_buffer,msg,query,xcoord,ycoord,length );
      temp_num = 0;
      count = string_num.length();
      for ( int i = 0 ; i < string_num.length() && valid_num; i ++ )
      {
      	if ( isdigit (string_num [i]) )
         {
	         count--;
   	   	temp_num += pow10 ( count ) * (int)(string_num[i]-'0');
			}
         else
				valid_num = false;
		}
   }
   while ( ! valid_num  );
   return temp_num;
}
//==========================================================================
bool Confirm ( BITMAP * draw_buffer, char * query, int xcoord, int ycoord )
{
   while ( TRUE )
   {
      poll_mouse ();
      poll_keyboard ();
		rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);   // 0 is black
		rect(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 15);		  // 15 is white
	   textprintf_centre(draw_buffer, font, xcoord + 100 , ycoord + 20, 15, query);
      textprintf(draw_buffer, font, xcoord + 50 , ycoord + 70, 15, "Yes");
      textprintf(draw_buffer, font, xcoord + 135 , ycoord + 70, 15, "No");
      show_mouse ( draw_buffer );
		blit(draw_buffer, screen, 0, 0, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

      if (key [KEY_ENTER])
      {
        	show_mouse (NULL);
			rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);
         show_mouse (draw_buffer);
         clear_keybuf();
   		return true;
       }
      else if (key [KEY_ESC])
      {
     	 	show_mouse (NULL);
  	  		rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);
        	show_mouse (draw_buffer);
         clear_keybuf();         
        	return false;
      }

	   if ( mouse_b & 1 )
      {
       	if ( (mouse_x >= xcoord + 50 && mouse_x <= xcoord+50+24)&&
         	  (mouse_y >= ycoord + 70 && mouse_y <= ycoord+70+8 ) )
			{
	        	show_mouse (NULL);
				rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);
            show_mouse (draw_buffer);
	   		return true;
			}
			else if ( (mouse_x >= xcoord + 135 && mouse_x <= xcoord+135+16)&&
         	  (mouse_y >= ycoord + 70 && mouse_y <= ycoord+70+8 ))
			{
      	 	show_mouse (NULL);
   	  		rectfill(draw_buffer, xcoord, ycoord, xcoord+200, ycoord+100, 0);
	        	show_mouse (draw_buffer);
         	return false;
			}
		}
   }
}
//==========================================================================
 void LoadStrings (apvector <apstring> & strings, char * file_name )
 {
 	int size;
   ifstream file;
   file.open ( file_name );
   file >> size;
   strings.resize ( size);
   for ( int index = 0; index < size; index++ )
   	file >> strings [index] ;
	file.close ();
 }
//==========================================================================
