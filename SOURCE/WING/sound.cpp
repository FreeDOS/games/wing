void stream_callback(void)
{
	if (stream_file && stream)
   {
		unsigned char *p = (unsigned char *)get_audio_stream_buffer(stream);
		if (p)
      {
	      if (feof(stream_file))
         {
				fseek(stream_file, 22, SEEK_SET);
				wfx.played = 0;
			}
 			if (wfx.played >= wfx.len - (STREAM_BUFFER_SIZE * wfx.bps))
        		memset(p, 0x80, STREAM_BUFFER_SIZE * wfx.bps);

	      wfx.played += fread(p, 1, STREAM_BUFFER_SIZE * wfx.bps, stream_file);
	   	free_audio_stream_buffer(stream);
   	}
	}
}
END_OF_FUNCTION(stream_callback);


void install_stream()
{
  LOCK_FUNCTION(stream_callback);
  LOCK_VARIABLE(stream);
  LOCK_VARIABLE(stream_file);
  LOCK_VARIABLE(wfx);
  stream_file  = NULL;
  stream = NULL;
}


void stream_start()
{
  install_int_ex(stream_callback, MSEC_TO_TIMER(250));
}


void stream_select_mfx( char *name )
{
	if (stream)
   {
		stop_audio_stream(stream);
		stream = NULL;
   }
   if (stream_file)
   {
		fclose(stream_file);
		stream_file = NULL;
   }
   stream_file = fopen(name,"rb");
   if (stream_file)
   {
		fgets( wfx.sig, 7, stream_file);
      wfx.len    = _getw(stream_file);
      wfx.bits   = _getw(stream_file);
      wfx.stereo = _getw(stream_file);
      wfx.freq   = _getw(stream_file);
      wfx.bps    = (wfx.bits / 8) * (wfx.stereo ? 2 : 1);
      wfx.played = 0;
      stream = play_audio_stream(STREAM_BUFFER_SIZE, wfx.bits, wfx.stereo, wfx.freq, stream_vol, 128);
	}
}


void stream_stop()
{
  fclose(stream_file);
  stream_file = NULL;
  stop_audio_stream(stream);
}


