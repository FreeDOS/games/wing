//****************************************************************************
void Init_Stars()
{
   // this function initializes all the stars
	for (int index=0; index < NUM_STARS; index++)
   {
   	// select random position
		stars[index].x = rand()%SCREEN_WIDTH;
    	stars[index].y = rand()%SCREEN_HEIGHT;

    	// set random velocity
    	stars[index].vel = 1 + rand()%16;
	}
}
//****************************************************************************
void Move_Stars()
{
	// this function moves all the stars and wraps them around the
	// screen boundaries
	for (int index=0; index < NUM_STARS; index++)
   {
    	// move the star and test for edge
   	stars[index].y+=stars[index].vel;

	    if (stars[index].y >= SCREEN_HEIGHT)
   	     stars[index].y -= SCREEN_HEIGHT;
   }
}
//****************************************************************************
void Draw_Stars(BITMAP * buffer)
{
	// this function draws all the stars
	for (int index=0; index < NUM_STARS; index++)
		putpixel( buffer, stars[index].x, stars[index].y, 15);
}
//****************************************************************************
