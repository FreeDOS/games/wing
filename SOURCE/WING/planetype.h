/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PlaneTYPE																				     *
 *                                                                           *
 * This class is handles the player controlled plane. It keeps track of 	  *
 * nearly everything associated with the player: score, health, weapon, 	  *
 * number of lives, etc. It also handles the movement and displaying of the  *
 * plane.                                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#ifndef _PlaneTYPE_H
#define _PlaneTYPE_H

#include <fstream.h>
#include "bullettype.h"
#include "animationtype.h"
#include "util.h"


class PlaneTYPE
{
	public :
   // Constructors & Destructors
      PlaneTYPE ( );
     ~PlaneTYPE ( );

   // Modifiers
		void SetScore      ( long int score );
      void MoveLeft      ( );
      void MoveRight     ( );
      void MoveUp        ( );
      void MoveDown      ( );
      bool Fire          ( );
      void Normalize     ( );
      void LoadPlane  	 ( char * PlaneF, char * BulletF, char * explodeF  );
      void SetDead 		 ( bool not_alive );
      void SetWeapon     ( WeaponEnum weapon );
		void SetHealth     ( int health );
      void SetNumLives    ( int new_lives );
      void Display 		 ( BITMAP * buffer );
      void Game_Defaults      ( );
      void Regen_Defaults      ( );      

      
   // Accessors
      int         GetImageIndex ( ) const;
      BITMAP *    GetImage      ( int index ) const;
      WeaponEnum  GetWeapon     ( ) const;
      bool			GetDead		  ( ) const;
      long int    GetScore      ( ) const;
      int         GetXPos       ( ) const;
      int         GetYPos       ( ) const;
      int         GetHealth     ( ) const;
      int 			GetNumLives   ( ) const;
      BulletTYPE *  GetBullets  ( );
		spr_mask  * GetMask       ( );

   private :
   	bool 					  dead;
      int                 myXPos;
      int                 myYPos;
      int                 myHealth;
      int                 myImageIndex;
      int 					  explode_stage;      
      WeaponEnum          myWeapon;
      BulletTYPE          myBullets;
      AnimationTYPE       myArrayOfImages;
      AnimationTYPE       explode_images;
      unsigned int	     last_shot_time;
      long int   			  myScore;
      int                 myNumLives;
      apvector <spr_mask > myPlaneMasks;
};

#include "planetype.cpp"
#endif
