/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * BulletTYPE																				     *
 *                                                                           *
 * This class provides the methods for dealing with the bullets used in the  *
 * game. It's primary functions (along with several helpers) are to display, *
 * to fire, and to provide access to individual bullets (because we wish to  *
 * treat them as individual objects as in real life)
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#ifndef _BulletTYPE_H
#define _BulletTYPE_H

#include "animationtype.h"
#include "collision.h"
#include "additional/apvector.h"
#include <fstream.h>
#include "util.h"

struct BulletNodeTYPE
{
	// data members
 		BulletNodeTYPE  * next;              // pointer to next node
      WeaponEnum  weapon;
	   int xPos;
   	int yPos;
	   int dx;
   	int dy;
	   bool visible;

	// constructors
   	BulletNodeTYPE ( BulletNodeTYPE * next_bullet = NULL )
 		: next( next_bullet ),
        weapon (laser),
        xPos (0),
        yPos (0),
        dx (0),
        dy (0),
        visible (true)
   	{  }
};

class BulletTYPE
{
   public :
	  	// constructors/destructor
   	BulletTYPE ();
      BulletTYPE ( char * fileName );
      ~BulletTYPE ();

      void LoadWeapon            ( char * fileName );
      bool Fire                  ( int xcoord, int ycoord, WeaponEnum weapon );
      bool Fire                  ( int xcoord, int ycoord, int dx, int dy, WeaponEnum weapon );
      void UpdateAndDisplayShot  ( BITMAP * buffer );	 // updates shot queue
      BulletNodeTYPE * GetBulletList   (  );
      BITMAP   * GetImage ( int index );
      spr_mask * GetMask ( int index );

   private :
   	int 		     myNumBullets;
	   BulletNodeTYPE  *   myBullets;
      AnimationTYPE myBulletImages;
      apvector <spr_mask > myBulletMask;
};

#include "bullettype.cpp"
#endif
