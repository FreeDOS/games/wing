/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * AnimationTYPE																				  *
 *                                                                           *
 * This class provides the methods for storing pictures, specifically groups *
 * of images that are logically linked together (e.g. all the cells in a     *
 * ship's animation).                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#ifndef AnimationTYPE_H
#define AnimationTYPE_H

#include "additional/apstring.h"
#include "additional/apvector.h"
#include <fstream.h>

class AnimationTYPE
{
	public :
   // CONSTRUCTOR / DESTRUCTOR
      AnimationTYPE ( );
      AnimationTYPE ( char * fileName );
     ~AnimationTYPE ( );

   // MODIFIER
      void       LoadImages ( char * fileName );

   // ACCESSOR
      BITMAP  *  GetImage   ( int Index ) const;
      RGB     *  GetPallete ( );
      int        Length     ( )           const;

   private :
      int mySize;
      PALLETE myPal;
      apvector < BITMAP* > myImageBank;
};

#include "animationtype.cpp"
#endif

