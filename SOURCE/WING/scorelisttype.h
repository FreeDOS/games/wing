/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ScoreListTYPE																				  *
 *                                                                           *
 * This class deals with the high score list. It reads and writes the list   *
 * to file, inserts new items, and displays. It uses a simple array to store *
 * the scores because the number of scores is known ahead of time.			  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#ifndef _SCORELISTTYPE_H
#define _SCORELISTTYPE_H

#include <iostream.h>
#include <fstream.h>
#include <string.h>
#include "util.h"
#include "additional/apvector.h"
#include "additional/apstring.h"

class ScoreListTYPE
{
	public:
		ScoreListTYPE ();
   	~ScoreListTYPE ();

	   void ResetList ();
   	void ReadFromFile ( char * file_name );
	   void InsertNewEntry ( const apstring & name, int score );
   	void DisplayList ( BITMAP * buffer);
	   void WriteToFile ( char * file_name );
      int  Lowest_Score ();

	private:
		struct ScoreTYPE
		{
  			apstring name;
	   	int score;

			ScoreTYPE ()
			{
				name  = "anonymous";
			   score = 0;
			}

			ScoreTYPE (const apstring & new_name, int new_score)
			{
				name = new_name;
			   score = new_score;
			}
		};
      apvector <ScoreTYPE> list;
};

#include "scorelisttype.cpp"
#endif
