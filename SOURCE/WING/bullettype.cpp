//***************************************************************************
BulletTYPE :: BulletTYPE ( )
: myBullets    ( NULL ),
  myNumBullets ( 0 ),
  myBulletImages ( )
{ }
//***************************************************************************
BulletTYPE :: BulletTYPE ( char * fileName )
: myBullets    ( NULL ),
  myNumBullets ( 0 )
{
	myBulletImages.LoadImages ( fileName );
}
//***************************************************************************
BulletTYPE :: ~BulletTYPE ()
{
	BulletNodeTYPE *temp;
	while ( myBullets != NULL )
   {
   	temp = myBullets;
      myBullets = myBullets->next;
      delete temp;
   }
}
//***************************************************************************
bool BulletTYPE :: Fire ( int xcoord, int ycoord, WeaponEnum new_weapon )
{
	return Fire ( xcoord,ycoord, XBulletVelocities [new_weapon], YBulletVelocities [new_weapon],new_weapon);
}
//***************************************************************************
bool BulletTYPE ::  Fire  ( int xcoord, int ycoord, int dx, int dy, WeaponEnum new_weapon )
{
	if ( myNumBullets < MAX_NUM_BULLETS )
   {
   	myNumBullets++;
		BulletNodeTYPE *NewShot = new BulletNodeTYPE;
	   NewShot->xPos = xcoord +35/2;
	   NewShot->yPos = ycoord + 5;
	   NewShot->visible = true;
	   NewShot->dx = dx;
	   NewShot->dy = dy;
      NewShot->weapon = new_weapon;
	 	NewShot->next = myBullets;
	   myBullets = NewShot;
      return true;
   }
   return false;
}
//***************************************************************************
void BulletTYPE :: UpdateAndDisplayShot (BITMAP * buffer)
{
	// we are putting the display with update because they are always called
   // together and we only want to go traverse the list once, speed wins out
   BulletNodeTYPE *current = myBullets;
   BulletNodeTYPE *previous = NULL;
   BulletNodeTYPE *DeleteNode = NULL;

	while ( current != NULL )
   {
    	current->yPos -= current->dy;
    	current->xPos -= current->dx;
      if ( current->yPos <= 0 || current->yPos >= SCREEN_HEIGHT)      // if the bullet goes off-screen delete it
      {
       	DeleteNode = current;
         current = current->next;
         if ( myBullets == DeleteNode )
				myBullets = DeleteNode->next;
         else
            previous->next = DeleteNode->next;
         myNumBullets--;
         delete DeleteNode;
      }
      else
      {
        if ( current->visible )
	        draw_sprite (buffer, myBulletImages.GetImage(current->weapon), current->xPos, current->yPos);
        previous = current;
        current = current->next;
      }
   }
}
//**************************************************************************
void BulletTYPE :: LoadWeapon ( char * fileName )
{
	myBulletImages.LoadImages ( fileName );
   myBulletMask.resize ( myBulletImages.Length() );
   init_sp_bb ( myBulletMask );

   for ( int i = 0; i < myBulletMask.length(); i++ )
	   mk_spr_mask (myBulletImages.GetImage(i), myBulletMask[i] );

}
//**************************************************************************
BulletNodeTYPE * BulletTYPE :: GetBulletList   (  )
{
	return myBullets;
}
//**************************************************************************
BITMAP  * BulletTYPE :: GetImage ( int index )
{
	return myBulletImages.GetImage ( index );
}
//**************************************************************************
spr_mask * BulletTYPE :: GetMask ( int index )
{
   return (&myBulletMask[index]);
}
//**************************************************************************
