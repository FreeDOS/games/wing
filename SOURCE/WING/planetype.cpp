
//========================================================================
PlaneTYPE :: PlaneTYPE ( )
: myXPos       ( 287 ),
  myYPos       ( 380 ),
  myHealth     ( 100 ),
  myImageIndex ( 0 ),
  explode_stage ( 0 ),  
  last_shot_time ( 0 ),
  myScore		(0),
  myNumLives   ( MAX_NUM_LIVES )
{ }
//========================================================================
PlaneTYPE :: ~PlaneTYPE ( )
{  }
//========================================================================
void PlaneTYPE :: LoadPlane ( char * PlaneF, char * BulletF, char * explodeF  )
{
	explode_images.LoadImages ( explodeF );
   myArrayOfImages.LoadImages ( PlaneF );
	myPlaneMasks.resize ( myArrayOfImages.Length() );
   init_sp_bb ( myPlaneMasks );
   for ( int i = 0; i < myPlaneMasks.length(); i++ )
   	mk_spr_mask ( myArrayOfImages.GetImage (i), myPlaneMasks [i] );
   myBullets.LoadWeapon ( BulletF );
}
//========================================================================
void PlaneTYPE :: SetWeapon  ( WeaponEnum new_weapon )
{
   myWeapon = new_weapon;
}
//========================================================================
int PlaneTYPE :: GetImageIndex ( ) const
{
	return myImageIndex;
}
//========================================================================
WeaponEnum PlaneTYPE ::  GetWeapon   ( ) const
{
    return myWeapon;
}
//========================================================================
int PlaneTYPE :: GetXPos       ( ) const
{
	return myXPos;
}
//========================================================================
int PlaneTYPE :: GetYPos       ( ) const
{
	return myYPos;
}
//========================================================================
int PlaneTYPE ::  GetHealth     ( ) const
{
	return myHealth;
}
//========================================================================
void PlaneTYPE :: SetHealth     ( int health )
{
	myHealth = health;
}
//========================================================================
void PlaneTYPE :: SetNumLives     ( int new_lives )
{
	myNumLives = new_lives;
}
//========================================================================
void PlaneTYPE :: SetScore ( long int score )
{
	myScore = score;
}
//========================================================================
long int PlaneTYPE :: GetScore ( ) const
{
	return myScore;
}
//========================================================================
bool PlaneTYPE :: GetDead ( ) const
{
	return dead;
}
//========================================================================
int PlaneTYPE :: GetNumLives ( ) const
{
	return myNumLives;
}
//========================================================================
void PlaneTYPE :: SetDead ( bool not_alive )
{
	dead = not_alive;
}
//========================================================================
void PlaneTYPE :: Display (  BITMAP * buffer )
{
	myBullets.UpdateAndDisplayShot ( buffer );
   if ( ! dead )
		draw_sprite ( buffer, myArrayOfImages.GetImage( myImageIndex ), myXPos, myYPos );
	else if ( explode_stage != PLANE_EXPLODE_STAGES - 1 )
   {
		draw_sprite ( buffer, explode_images.GetImage( explode_stage ), myXPos, myYPos );
		explode_stage ++;
   }
   else
		Regen_Defaults ( );
}
//========================================================================
void PlaneTYPE :: MoveLeft ( )
{
	if ( myXPos >= 0)
   {
   	myXPos-= X_VELOCITY;
      if ( myImageIndex > 0 )
        myImageIndex--;
   }
}
//========================================================================
void PlaneTYPE :: MoveRight ( )
{
   if (myXPos <= SCREEN_WIDTH-75)
   {
   	myXPos+= X_VELOCITY;
      if ( myImageIndex < myArrayOfImages.Length()-1 )
        myImageIndex++;
   }
}
//=========================================================================
void PlaneTYPE :: MoveUp   ( )
{
	if (myYPos >= 0 )
     myYPos-= Y_VELOCITY;
}
//========================================================================
void PlaneTYPE :: MoveDown ( )
{
  if ( myYPos <= SCREEN_HEIGHT-75 )
      myYPos+= Y_VELOCITY;
}
//=======================================================================
bool PlaneTYPE :: Fire ( )
{
	if ( timer - last_shot_time > BULLET_PAUSE )
   {
   	last_shot_time = timer;
		return myBullets.Fire ( myXPos, myYPos,myWeapon );
	}
	return false;
}
//=======================================================================
void PlaneTYPE :: Normalize      ( )
{
	if ( myImageIndex != myArrayOfImages.Length()/2 )
     if ( myImageIndex > myArrayOfImages.Length()/2 ) myImageIndex--;
       else myImageIndex++;
}
//=======================================================================
BITMAP * PlaneTYPE ::  GetImage ( int index ) const
{
	return myArrayOfImages.GetImage ( index );
}
//=======================================================================
BulletTYPE * PlaneTYPE ::  GetBullets   (  )
{
	return  (& myBullets) ;
}
//=======================================================================
spr_mask  * PlaneTYPE :: GetMask ( )
{
	return (&myPlaneMasks[myImageIndex]);
}
//=======================================================================
void PlaneTYPE :: Game_Defaults ( )
{
	myNumLives = MAX_NUM_LIVES ;
   myScore = 0 ;
   Regen_Defaults ( );
}
//=======================================================================
void PlaneTYPE :: Regen_Defaults ( )
{
   myXPos = 287;
   myYPos = 380;
   myWeapon = laser;
   myImageIndex = myArrayOfImages.Length () / 2;
   dead = false;
   explode_stage = 0;
}
//=======================================================================
