/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EnemyTYPE																				     *
 *                                                                           *
 * This class provides the methods for keeping track of the linked list of   *
 * enemies that is loaded with each succesive level of play in the game. 	  *
 * It is responsible for AI and for updating the list with each new level of *
 * the game.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#ifndef _ENEMYTYPE_H
#define _ENEMYTYPE_H

#include <fstream.h>
#include "bullettype.h"
#include "animationtype.h"
#include "util.h"

		struct EnemyNodeTYPE
      {
	  		int            xpos, ypos, dx,dy,health;
		  	bool				attacking;
		  	bool				dead;         
   		unsigned int   TimeOfLastFired;
	  		WeaponEnum     weapon;
   		EnemyEnum      TypeOfEnemy;
         StateEnum      state;
         int 				explode_stage;
         int 				state_stage;
	 		EnemyNodeTYPE  *  next;              // pointer to next node
		   EnemyNodeTYPE  *  back;              // pointer to previous node

   	 	// constructors

	 	   EnemyNodeTYPE (EnemyNodeTYPE * back_link = NULL,EnemyNodeTYPE * next_link = NULL)
		   :  next    			 ( next_link ),
            back    			 ( back_link ),
            attacking		 ( false ),
            dead				 ( false ),
            TimeOfLastFired ( 0 ),
            weapon          ( laser ),
            TypeOfEnemy     ( scout ),
            xpos 				 ( rand () % ( SCREEN_WIDTH - 72) ),
            ypos 				 ( INIT_ENEMY_Y ),
            dx 				 ( 1 ),
            dy 				 ( 1 ),
            health			 ( 0 ),
            state           ( IntToState (rand ()%TOTAL_NUM_STATES) ),
				state_stage  	 ( 0 ),
            explode_stage   ( 0 )
 	     	{  }

         void ResetNode ( )
         {
				xpos = rand () % ( SCREEN_WIDTH - 72);
            ypos = INIT_ENEMY_Y;
            dx = dy = 1;
            state = IntToState (rand ()%TOTAL_NUM_STATES);
            state_stage = TimeOfLastFired = 0; 
         }
		};

class EnemyTYPE
{
	public :
   // Constructors & Destructors
      EnemyTYPE ( );
     ~EnemyTYPE ( );

   // Modifiers
      void LoadEnemies 	       ( char * level_file, char * sprite_file, char * bullet_file , char * explode_file  );
      void UpdateAI            ( int plane_x, int plane_y );
      void AddAttacker         ( );
      void DestroyEnemies 		 ( );
      void SetHealth           ( int index, int new_health );
      

   // Accessors
      void Display 		       ( BITMAP * buffer );
      int  GetTotalNumEnemies  ( );
		int  GetNumAttackers     ( );
      int  GetNumEnemiesLeft   ( );
      int  GetHealth 			 ( int index );
      EnemyNodeTYPE  *  GetEnemyList ( );
      BulletTYPE     *  GetBullets ( );

   // Public Data
      apvector <spr_mask> myEnemyMasks;

   private :
   	// private member functions
		void Attack_1 ( EnemyNodeTYPE * enemy );
		void Attack_2 ( EnemyNodeTYPE * enemy );
		void Attack_3 ( EnemyNodeTYPE * enemy ,int plane_x);
		void Attack_4 ( EnemyNodeTYPE * enemy );
  		void Attack_5 ( EnemyNodeTYPE * enemy );
  		void Attack_6 ( EnemyNodeTYPE * enemy );
      void DeleteNode          ( EnemyNodeTYPE * scan );

   	// private data members
      int num_enemies_left;
      int num_enemies_attacking;
      int num_enemies_per_level;
      EnemyNodeTYPE * most_recent;          // ####################### is this necessary ???
      BulletTYPE 		 enemy_bullets;
      AnimationTYPE   enemy_images;
      AnimationTYPE   explode_images;
      EnemyNodeTYPE * enemy_list;
};

#include "enemytype.cpp"
#endif